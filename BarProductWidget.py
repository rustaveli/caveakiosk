# -*- coding: utf-8 -*-
import sys
import random
from PySide6 import QtCore, QtWidgets, QtGui
from urllib.request import *
import urllib.parse
import globals

class BarProductWidget(QtWidgets.QWidget):

    def __init__(self):
        super().__init__()

        self.temp =  [{"price": 2.0, "name": "\u10ec\u10e7\u10d0\u10da\u10d8", 
                        "image": "https://cinerp.com/media/product/images/thumbnail/0724a2f5-e2c6-485c-9a31-42872aa90596download (7).jpg", "product_id": "CGT004"}, 
                        {"price": 4.2, "name": "\u10de\u10d4\u10de\u10e1\u10d8 \u10de\u10d0\u10e2\u10d0\u10e0\u10d0", 
                        "image": "https://cinerp.com/media/product/images/thumbnail/62f1158b-6314-4d71-b561-158081edc127pepsi.gif", "product_id": "CGT008"}, 
                        {"price": 5.9, "name": "\u10de\u10d4\u10de\u10e1\u10d8 \u10e1\u10d0\u10e8\u10e3\u10d0\u10da\u10dd", 
                        "image": "https://cinerp.com/media/product/images/thumbnail/4825789c-665d-402f-b017-0e3fba98b96cpepsi.gif", "product_id": "CGT009"}, 
                        {"price": 6.9, "name": "\u10de\u10d4\u10de\u10e1\u10d8 \u10d3\u10d8\u10d3\u10d8", 
                        "image": "https://cinerp.com/media/product/images/thumbnail/b2f8840d-62f3-4f53-a81b-8b790edc176bpepsi.gif", "product_id": "CGT010"}, 
                        {"price": 4.8, "name": "\u10de\u10dd\u10de\u10d9\u10dd\u10e0\u10dc\u10d8 \u10de\u10d0\u10e2\u10d0\u10e0\u10d0", 
                        "image": "https://cinerp.com/media/product/images/thumbnail/30140aa2-ba74-4c6c-a455-e98cf582590924337431_1720969271267191_1447769383_n.jpg", "product_id": "CGT011"}, 
                        {"price": 7.0, "name": "\u10de\u10dd\u10de\u10d9\u10dd\u10e0\u10dc\u10d8 \u10e1\u10d0\u10e8\u10e3\u10d0\u10da\u10dd", 
                        "image": "https://cinerp.com/media/product/images/thumbnail/a6cd8ff6-0d50-4ba2-9b3a-452a7091533824337431_1720969271267191_1447769383_n.jpg", "product_id": "CGT012"}, 
                        {"price": 9.0, "name": "\u10de\u10dd\u10de\u10d9\u10dd\u10e0\u10dc\u10d8 \u10d3\u10d8\u10d3\u10d8", 
                        "image": "https://cinerp.com/media/product/images/thumbnail/2d2c1d65-14c7-4d87-873f-b70e6ef869d224337431_1720969271267191_1447769383_n.jpg", "product_id": "CGT013"}, 
                        {"price": 10.5, "name": "\u10d9\u10d0\u10e0\u10d0\u10db\u10d4\u10da-\u10e8\u10dd\u10d9\u10dd\u10da\u10d0\u10d3\u10d8 \u10d3\u10d8\u10d3\u10d8", 
                        "image": "https://cinerp.com/media/product/images/thumbnail/ac9a23cb-dec0-4ff9-85c1-b88079c816fb24331203_1720969264600525_1907385655_n.jpg", "product_id": "CGT024"}, 
                        {"price": 8.5, "name": "\u10d9\u10d0\u10e0\u10d0\u10db\u10d4\u10da-\u10e8\u10dd\u10d9\u10dd\u10da\u10d0\u10d3\u10d8 \u10e1\u10d0\u10e8\u10e3\u10d0\u10da\u10dd", 
                        "image": "https://cinerp.com/media/product/images/thumbnail/c9bea5c5-8df3-4ebc-a961-5db313ee311c24331203_1720969264600525_1907385655_n.jpg", "product_id": "CGT025"}, 
                        {"price": 6.3, "name": "\u10d9\u10d0\u10e0\u10d0\u10db\u10d4\u10da-\u10e8\u10dd\u10d9\u10dd\u10da\u10d0\u10d3\u10d8 \u10de\u10d0\u10e2\u10d0\u10e0\u10d0", 
                        "image": "https://cinerp.com/media/product/images/thumbnail/baca476c-6ccf-4b99-975c-a32fbae4e50b24331203_1720969264600525_1907385655_n.jpg", "product_id": "CGT026"}, 
                        {"price": 5.0, "name": "\u10d1\u10d0\u10dd", 
                        "image": "https://cinerp.com/media/", "product_id": "CGT031"}, 
                        {"price": 9.0, "name": "\u10dc\u10d0\u10e9\u10dd", 
                        "image": "https://cinerp.com/media/product/images/thumbnail/1bbf07ce-0072-4548-aeac-b217b68311dd1975041420.jpg", "product_id": "CGT038"}, 
                        {"price": 2.0, "name": "3D \u10e1\u10d0\u10d7\u10d5\u10d0\u10da\u10d4 \u10d3\u10d8\u10d3\u10d8", 
                        "image": "https://cinerp.com/media/product/images/thumbnail/b9dc77a7-e815-47aa-a6e5-d07e9c68a9baae4uw.jpg", "product_id": "CGT047"}]

        
        self.text = QtWidgets.QLabel("Product Title",
                                     alignment=QtCore.Qt.AlignCenter)
        self.poster = QtWidgets.QLabel()

        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.addWidget(self.text)
        self.layout.addWidget(self.poster)

        self.id = ''
        self.nameGeo = ''
        self.nameEng = ''
        self.desc_geo = ''
        self.desc_eng = ''
        self.imageURL = ''
        self.banner = ''
        self.price = ''

        self.setWindowTitle('BarWidget')

    def setPoster(self,data):
        self.text.setText(data['product_id']+ ' ' + data['name'])
        url = data['image']
        print(url)
        try:
            dataImage = urlopen(urllib.parse.quote(url,safe=':/')).read()
            pixMap = QtGui.QPixmap()
            pixMap.loadFromData(dataImage)
            self.poster.setPixmap(pixMap)
            self.poster.setScaledContents(True)
        except:
            pass

    def mousePressEvent(self,event):
        print ("Pressed")


    @QtCore.Slot()
    def posterClicked(self):
        print("Clicked")


if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = BarProductWidget()
    widget.show()

    sys.exit(app.exec_())