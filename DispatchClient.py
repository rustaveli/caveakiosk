import urllib
from urllib.request import *
from urllib.error import *
from urllib.parse import *
import json
from pprint import pprint
from http.cookiejar import CookieJar
import globals
from DispatchSocket.DispatchSocketClient import DispatchSocketClient
import threading
from threading import Thread
import time


class DClient:
    SERVER_IP =  globals.config.get('dispatch', 'SERVER_IP')
    GlobalToken = "None"
    GlobalCookie = "None"
    GlobalSession = "None"
    Sock = None

    def updateDClientAddress(self,newAddress):
        DClient.SERVER_IP = newAddress

    def getlistofagents(self):
        fullURL = "%s/getlistofboxagents.api" % (DClient.SERVER_IP)
        headers = {'csrfmiddlewaretoken':'None'}
        req = Request(fullURL, None, headers)
        print('UserAgents API: ', fullURL)
        return urlopen(req).read()

    def loginUser(self, username, password, agent):
        url = "%s/authenticate.api/" % (DClient.SERVER_IP)
        values =  {'username' : username, 'password'  : password, 'agent':agent}

        cj = CookieJar()
        opener = build_opener(HTTPCookieProcessor(cj))
        data = urlencode(values).encode('utf-8')
        #response = opener.open(url, data)
        install_opener(opener)
        response = urlopen(url,data)
        responseText = ''

        globals.CoookieFile = cj


        try:
            responseText = response.read()
            response = json.loads(responseText)
            print(response)
            if(response['status'] == 'Failed'):
                return response

            DClient.GlobalToken = response['token']
            DispatchSocketClient.user = username
            DispatchSocketClient.token = DClient.GlobalToken
            auth_data = json.loads(responseText)

            print ('Dispatch Auth Data: ', auth_data)

            globals.bar_user_id = auth_data["user_id"]
            globals.bar_user_level = auth_data["user_level"]
            globals.bar_user_name = username
            print (auth_data)
        except HTTPError as e:
            print (e.fp.read())
        #print "login response ", responseText
        return response

    def getCinemas(self,for_user=None):
        fullURL = "%s/theaternew.api" % (DClient.SERVER_IP)
        if for_user:
            fullURL = '%s/theaternew.api/?user=%s' % (DClient.SERVER_IP, for_user)
        headers = {'csrfmiddlewaretoken':DClient.GlobalToken}
        req = Request(fullURL, None, headers)
        response = urlopen(req).read()
        return response


    def getMovies(self,theater_id,for_user=None):
        fullURL = "%s/v3/webmovies.api/?api_key=ding" % (DClient.SERVER_IP)
        if for_user and theater_id:
            fullURL = '%s/v3/webmovies.api/?api_key=ding&theater_id=%s&user=%s' % (DClient.SERVER_IP, theater_id,for_user)
        elif theater_id:
            fullURL = '%s/v3/webmovies.api/?api_key=ding&theater_id=%s' % (DClient.SERVER_IP, theater_id)
        headers = {'csrfmiddlewaretoken':DClient.GlobalToken}
        req = Request(fullURL, None, headers)
        response = urlopen(req).read()
        return response

    def getSessions(self,theater_id,movie_id):
        fullURL = "%s/v3/websessions.api/%s/%s/?api_key=key" % (DClient.SERVER_IP ,theater_id,movie_id)
        headers = {'csrfmiddlewaretoken':DClient.GlobalToken}
        req = Request(fullURL, None, headers)
        respo = urlopen(req).read()
        #print(respo)
        return respo

    def getSeats(self,hall_id):
        fullURL = "%s/v3/webseats.api/?api_key=%s" % (DClient.SERVER_IP,"123")
        data = {'hall_id' : hall_id, 'csrfmiddlewaretoken':DClient.GlobalToken}
        print (fullURL)
        return self.addCookiesAndPerformRequest(fullURL, data)

    def getSeatStates(self,session_id):
        fullURL = "%s/v3/webseatstates.api/?api_key=%s" % (DClient.SERVER_IP,"123")
        data = {'session_id' : session_id, 'csrfmiddlewaretoken':DClient.GlobalToken}
        print (fullURL)
        return self.addCookiesAndPerformRequest(fullURL, data)



    def getComingMovies(self,for_user=None):
        fullURL = "%s/v3/webmoviescoming.api/?api_key=ding" % (DClient.SERVER_IP)
        if for_user:
            fullURL = '%s/v3/webmoviescoming.api/?api_key=ding&user=%s' % (DClient.SERVER_IP, for_user)
        headers = {'csrfmiddlewaretoken':DClient.GlobalToken}
        req = Request(fullURL, None, headers)
        response = urlopen(req).read()
        return response

    def getBarProducts(self,for_user=None):
        fullURL = "%s/v3/webgetbarproducts.api/?api_key=a&theater_id=5a1583c9956167428b722f81" % (DClient.SERVER_IP)
        if for_user:
            fullURL = '%s/v3/webgetbarproducts.api/?api_key=a&theater_id=5a1583c9956167428b722f81&user=%s' % (DClient.SERVER_IP, for_user)
        headers = {'csrfmiddlewaretoken':DClient.GlobalToken}
        req = Request(fullURL, None, headers)
        response = urlopen(req).read()
        return response



    def getFilmDescription(self,movieID):
        fullURL = "%s/filmdetails.api/" % (DClient.SERVER_IP)
        data = {'movieID'  : movieID,'csrfmiddlewaretoken':DClient.GlobalToken}
        return self.addCookiesAndPerformRequest(fullURL, data)

    def getSessionsForHall(self, hallID):
        fullURL = "%s/sessionNew.api/%s" % (DClient.SERVER_IP ,hallID)
        print ('getSessionsForHall   ' + fullURL)
        headers = {'csrfmiddlewaretoken':DClient.GlobalToken}
        req = Request(fullURL, None, headers)
        respo = urlopen(req).read()
        print ("="*20)
        print (respo)
        print ("="*20)
        return respo

    def getSessionCleanData(self,session_id):
        fullURL = "%s/getcleansession.api/" % (DClient.SERVER_IP)
        data = {'session_id' : session_id, 'csrfmiddlewaretoken':DClient.GlobalToken}
        return self.addCookiesAndPerformRequest(fullURL, data)

    def getNewSessionsForMovieInCinema(self, cinemaID, movieID):
        fullURL = "%s/getsessionsbymovieincinema.api/%s/%s" % (DClient.SERVER_IP ,cinemaID, movieID)
        headers = {'csrfmiddlewaretoken':DClient.GlobalToken}
        req = Request(fullURL, None, headers)
        return urlopen(req).read()

    def getAllSessionsForHall(self, hallID):
        fullURL = "%s/session.api/%s" % (DClient.SERVER_IP ,hallID)
        print ('getAllSessionsForHall : ' + fullURL)
        headers = {'csrfmiddlewaretoken':DClient.GlobalToken}
        req = Request(fullURL, None, headers)
        return urlopen(req).read()

    def getSessionsForDay(self, cinemaID, delta):
        fullURL = "%s/sessionforday.api/" % (DClient.SERVER_IP)
        data = {'cinema_id' : cinemaID, 'time_delta'  : delta, 'csrfmiddlewaretoken':DClient.GlobalToken}
        print (fullURL)
        return self.addCookiesAndPerformRequest(fullURL, data)

    def getSpecificSession(self,session_id):
        fullURL = "%s/specificsession.api/" % (DClient.SERVER_IP)
        data = {'session_id' : session_id, 'csrfmiddlewaretoken':DClient.GlobalToken}
        print (fullURL)
        return self.addCookiesAndPerformRequest(fullURL, data)



    def getSeatStatesForSession(self, sessionID):

        DispatchSocketClient.session = sessionID

        if not DClient.Sock:
            DClient.Sock = DispatchSocketClient()
            #DClient.Sock.startSocket()
            t = threading.Thread(target=DClient.Sock.startSocket, args = ([]))
            t.daemon = True
            t.start()
        else:
            DClient.Sock.subscribe_to_session()        

        fullURL = "%s/seatstates.api/%s" % (DClient.SERVER_IP ,sessionID)
        print ('getSeatStatesForSession ', fullURL)
        headers = {'csrfmiddlewaretoken':DClient.GlobalToken}
        req = Request(fullURL, None, headers)
        response = urlopen(req).read()
        
        #print response
        return response


    def setSeatSelected(self, sessionID, seatID, seat_select_type=None):
        fullURL = "%s/ticketselect.api/"% DClient.SERVER_IP

        seatIDsArray = []
        seatIDsArray.append(seatID)

        values =  {
                    'session_id' : str(sessionID),
                    'seats'  : seatID,
                    'csrfmiddlewaretoken': DClient.GlobalToken
        }
        if seat_select_type:
            values['block_type'] = seat_select_type
        #print 'type: ', type(values)
        #pprint(values)
        return self.addCookiesAndPerformRequest(fullURL, values)

    def seatsUnselect(self, sessionID, seatIDsArray):
        #print 'Seat Unselect !!! '
        fullURL = "%s/TicketUnselectAPI/"% DClient.SERVER_IP
        seatsdumpedArray = json.dumps(seatIDsArray)
        values =  {'session_id' : str(sessionID), 'seats'  :  seatsdumpedArray , 'csrfmiddlewaretoken':DClient.GlobalToken}

        return self.addCookiesAndPerformRequest(fullURL,  values )

    def returnTickets(self, sessionID, seatIDsArray, token=None,cancel_note=None):
        url = "%s/ticketcancel.api/" % (DClient.SERVER_IP)
        values =  {'session_id' : sessionID, 'seats'  : seatIDsArray, 'csrfmiddlewaretoken':DClient.GlobalToken,'cancel_note':cancel_note}
        return self.addCookiesAndPerformRequest(url, values)

    def buyTicketsURLIB(self, sessionID, seatIDsArray, boxOfficeProducsArray,paymentInfo, cartData,discount_note=None,unipay=None):

        url = "%s/TicketBuyAPI/" % (DClient.SERVER_IP)
        if len(boxOfficeProducsArray)==0:
            boxOfficeProducsArray = '["None"]'

        giftCardCode = ''
        if(paymentInfo['giftCardCode'] is not None):
            giftCardCode = paymentInfo['giftCardCode']

        values = {
                'csrfmiddlewaretoken':DClient.GlobalToken,
                'session_id' : sessionID,
                'seats'  : seatIDsArray,
                'startAmount': paymentInfo['startAmount'],
                'discountAmount': paymentInfo['discountAmount'],
                'paidByCash' : paymentInfo['paidByCash'],
                'paidByCreditCard': paymentInfo['paidByCreditCard'],
                'paidByGiftCard' : paymentInfo['paidByGiftCard'],
                'paidByWire' : paymentInfo['paidByWire'],
                'giftCardCode': giftCardCode,
                'finalAmount': paymentInfo['finalAmount'],
                'boxOfficeProducts': boxOfficeProducsArray,
                'reservationPin' : paymentInfo['reservationPin'],
                'discount_id': paymentInfo['discount_id'],
                #'cartData': cartData,
                'card_payment_log' : paymentInfo['card_payment_log'],
                'discount_note':discount_note,
                'unipay':unipay
        }

        # print 'Final Buy Request: '
        # print '-'*100
        # pprint(values)
        # print '-'*100

        response = self.addCookiesAndPerformRequest(url, values)
        # print response
        return response

    
    def addCookiesAndPerformRequest(self, url, values):
        opener = build_opener(HTTPCookieProcessor(globals.CoookieFile))

        data = bytes(urlencode(values),'utf-8')
        install_opener(opener)
        response = urlopen(url,data)
        #response = opener.open(url, data)
        responseText = ''
        try:
            responseText = response.read()
            #print responseText
        except HTTPErro as e:
            print (e.fp.read())
        #print "Response ", responseText
        return responseText

    

from PySide6.QtCore import *
class SocketConnectionThread(QThread):
    def __init__(self, parent=None):
        super(SocketConnectionThread, self).__init__(parent)

    def run(self):
        print ('Run socket connection thread ')
        if not DClient.Sock:
            DClient.Sock = DispatchSocketClient()
            DClient.Sock.startSocket()
        else:
            DClient.Sock.subscribe_to_session()



        #self.emit(SIGNAL("LoadSessionFinished(QString)"), sessions)







