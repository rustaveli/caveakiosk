# -*- coding: utf-8 -*-
import sys
import random
from PySide6 import QtCore, QtWidgets, QtGui
from urllib.request import *
import datetime
import time

class TimerWidget(QtWidgets.QWidget):

   

    def __init__(self):
        super().__init__()
        self.setGeometry(300, 300, 350, 350)
        self.setWindowTitle('Draw circles')

        self.innerrady = 0
        self.start = datetime.datetime.now()

        

    def setrady(self,value): 
        self.innerrady = value
        self.timerLabel.setText(str((int)(float(value)/360*16)*self.duration))
        self.update()
    def getrady(self): return self.innerrady

    rady = QtCore.Property(int,getrady,setrady)

    def paintEvent(self, event):
        #print("paint event")
        paint = QtGui.QPainter()

        paint.begin(self)
        # optional
        paint.setRenderHint(QtGui.QPainter.Antialiasing)
        # make a white drawing background
        paint.setBrush(QtGui.QColor(255, 255, 255, 0))
        paint.drawRect(event.rect())
        # for circle make the ellipse radii match
        radx = 90*16
        # draw red circles
        paint.setPen(QtGui.QColor(255, 0, 0, 255))
        paint.setBrush(QtGui.QColor(204, 0, 102, 255))
        paint.drawPie(event.rect(),radx,-self.rady)

        #print (self.rady)
        paint.setPen(QtGui.QColor(0, 0, 0, 255))
        paint.setFont(QtGui.QFont("Arial", 90))
        paint.drawText(QtCore.QRect(50,100,200,200), 3,str(self.duration - int((self.rady/5760)*self.duration)))
        # for k in range(125, 220, 10):
        #     center = QtCore.QPoint(k, k)
        #     # optionally fill each circle yellow
        #     paint.setBrush(QtGui.QColor(100, 100, 0, 255))
        #     paint.drawPie(event.rect(),radx,rady)
        #     #paint.drawEllipse(center, radx, rady)
        paint.end()

    def animated(self,value): print("animating"); self.update()

    @QtCore.Slot()
    def startCountdown(self, duration):
        self.timerLabel = QtWidgets.QLabel(self)
        self.timerLabel.setText(str(duration))

        self.duration = duration

        self.animation = QtCore.QPropertyAnimation(self,b"rady")
        self.animation.setDuration(self.duration*1000)
        self.animation.setStartValue(10)
        self.animation.setEndValue(360*16)
        self.animation.start()
        #print("Done")



if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = TimerWidget()
    widget.show()
    # widget.startCountdownSignal.connect(widget.startCountdown)
    # widget.startCountdownSignal.emit()
    #time.sleep(1)
    widget.startCountdown(60)

    sys.exit(app.exec_())