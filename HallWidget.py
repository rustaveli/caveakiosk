# -*- coding: utf-8 -*-
import sys
import random
from PySide6 import QtCore, QtWidgets, QtGui
from urllib.request import *
import datetime

class HallWidget(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)


    def drawHall(self,hallData,scaleFactor):
        self.closeButton = QtWidgets.QPushButton(self)
        self.closeButton.setText("X")
        self.closeButton.setStyleSheet("background-color: grey;")
        self.closeButton.clicked.connect(self.closeHallWidget)
        self.closeButton.setGeometry(10,10,20,20)

        offset = 30

        self.seatDict = {}
        for seat in hallData:
            seatButton = QtWidgets.QPushButton(self)
            x=seat['left'] * scaleFactor + offset
            y=seat['top'] * scaleFactor + offset
            width=seat['width'] * scaleFactor
            height=seat['width'] * scaleFactor
            row=seat['row']
            st=seat['seat']
            ttype=seat["ttype"]
            sid=seat["_id"]
            seatButton.setGeometry(x,y,width,height)
            seatButton.setText(str(st))
            seatButton.ttype = ttype

            if ttype == 'vip':
                seatButton.setStyleSheet("background-color: rgb(131, 93, 193); border-radius:5px;")
            elif ttype == 'sta':
                seatButton.setStyleSheet("background-color: grey; border-radius:5px;")
            elif ttype == 'std':
                seatButton.setStyleSheet("background-color: white;border-radius:5px;")
            elif ttype == 'eco':
                seatButton.setStyleSheet("background-color: white;border-radius:5px;")
            else:
                seatButton.setStyleSheet("background-color: white;border-radius:5px;")

            seatButton.pk=sid
            self.seatDict[sid] = seatButton

            self.drawLegend()

    def drawLegend(self):
        #draw movie title, time, language, other info

        #draw seats and prices
        self.legend = QtWidgets.QWidget(self)
        self.legend.setGeometry(0,700,1000,50)
        self.legend.setAttribute(QtCore.Qt.WA_StyledBackground, True)
        self.legend.setStyleSheet("background-color: grey;border-radius:5px;")

        vipseat = QtWidgets.QPushButton(self.legend)
        vipseat.setStyleSheet("background-color: rgb(131, 93, 193);border-radius:5px;")
        vipseat.setGeometry(30,0,30,30)

        stdseat = QtWidgets.QPushButton(self.legend)
        stdseat.setStyleSheet("background-color: white;border-radius:5px;")
        stdseat.setGeometry(70,0,30,30)

        staseat = QtWidgets.QPushButton(self.legend)
        staseat.setStyleSheet("background-color: grey;border-radius:5px;")
        staseat.setGeometry(110,0,30,30)

        freeseat = QtWidgets.QPushButton(self.legend)
        freeseat.setStyleSheet("background-color: white;border-radius:5px;")
        freeseat.setGeometry(150,0,30,30)

        self.legend.show()

    def closeHallWidget(self):
        self.hide()

    def setSeatStates(self,data):
        for seat in data:
            seatButton = self.seatDict[seat['seat_id']]
            seatButton.setStyleSheet("background-color: rgba(204, 51, 153, 250);")
            seatButton.clickable = False


if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = HallWidget()
    widget.show()

    sys.exit(app.exec_())