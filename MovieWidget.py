# -*- coding: utf-8 -*-
import sys
import random
from PySide6 import QtCore, QtWidgets, QtGui
from urllib.request import *

class MovieWidget(QtWidgets.QWidget):

    getSessionsSignal = QtCore.Signal(str)

    def __init__(self):
        super().__init__()

        self.temp = {"poster": "https://cinerp.com/media/movies/posters/images/mvv.jpg", "duration": 0, 
                    "banner": "https://cinerp.com/mediamovies/posters/banners/mh.jpg", "id": {"$oid": "5e43af9c9561678d1d2e1180"}, 
                    "desc_geo": {"rating": 0.0, "description": "", "premiere": "", 
                    "directors": "\\u10dc\\u10d8\\u10d9\\u10d8 \\u10d9\\u10d0\\u10e0\\u10dd", 
                    "actors": "\\u10da\\u10d8\\u10e3 \\u10d8\\u10e4\\u10d4\\u10d8, \\u10d3\\u10dd\\u10dc\\u10d8 \\u10d8\\u10d4\\u10dc\\u10d8, \\u10ef\\u10d4\\u10e2 \\u10da\\u10d8, \\u10d2\\u10e3\\u10dc \\u10da\\u10d8", 
                    "imdb_score": 0.0, "genre": "\\u10db\\u10eb\\u10d0\\u10e4\\u10e0\\u10e1\\u10d8\\u10e3\\u10df\\u10d4\\u10e2\\u10d8\\u10d0\\u10dc\\u10d8, \\u10e1\\u10d0\\u10d7\\u10d0\\u10d5\\u10d2\\u10d0\\u10d3\\u10d0\\u10e1\\u10d0\\u10d5\\u10da\\u10dd,\\u10d3\\u10e0\\u10d0\\u10db\\u10d0"}, 
                    "name": "Mulan", "desc_eng": {"rating": 0.0, "description": "", "premiere": "", "directors": "Niki Caro", 
                    "actors": " Yifei Liu, Donnie Yen, Jet Li ", "imdb_score": 0.0, "genre": "Action, Adventure, Drama "}, 
                    "is_festival": False, "mpaa_rating": "PG", "name_eng": "Mulan", "position": 1, 
                    "trailer_url": "https://www.youtube.com/watch?v=XrAmQS9jJJo", "thumbnail": "https://cinerp.com/mediamovies/posters/thumbnails/mvv.jpg"
                    }

        
        self.poster = QtWidgets.QLabel(alignment=QtCore.Qt.AlignCenter)
        self.text = QtWidgets.QLabel("Movie Title",
                                     alignment=QtCore.Qt.AlignCenter)
        #self.poster.setSizePolicy(QtWidgets.QSizePolicy.Maximum,QtWidgets.QSizePolicy.Maximum)
        self.text.setSizePolicy(QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Maximum)
        

        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.addWidget(self.poster)
        self.layout.addWidget(self.text)

        self.data = ''
        self.id = ''
        self.nameGeo = ''
        self.nameEng = ''
        self.genre = ''
        self.desc_geo = ''
        self.desc_eng = ''
        self.posterURL = ''
        self.banner = ''
        self.duration = ''
        

        self.setWindowTitle('MovieWidget')

    def setPoster(self,data):
        self.data = data
        self.text.setText(data['name_eng'])
        self.id = data['id']['$oid']
        dataPoster = urlopen(data['poster']).read()
        pixMap = QtGui.QPixmap()
        pixMap.loadFromData(dataPoster)
        self.poster.setPixmap(pixMap)
        self.poster.setScaledContents(True)

    def mousePressEvent(self,event):
        print ("Pressed Movie "+self.data['name_eng']+ " With ID "+self.data['id']['$oid'])
        #self.GetSessionsSignal.emit(self.data)
        self.getSessionsSignal.emit(self.id)

    @QtCore.Slot()
    def posterClicked(self):
        print("Clicked")


if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = MovieWidget()
    widget.show()

    sys.exit(app.exec_())