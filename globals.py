# -*- coding: utf-8 -*-
import configparser
import os
import platform
from PySide6 import QtCore, QtWidgets
from PySide6.QtCore import QObject

config = configparser.RawConfigParser()
if platform.system()=='Windows':
    config.read('c:\\rq\\default.cfg',encoding='utf-8')
else:
    print("OS is not windows")
    config.read('/Users/default.cfg',encoding='utf-8')


print(config['dispatch']['SERVER_IP'])

class CQPushButton(QtWidgets.QPushButton):
    updateStyleSignal = QtCore.Signal(QObject,str)

class InvokeEvent(QtCore.QEvent):
    EVENT_TYPE = QtCore.QEvent.Type(QtCore.QEvent.registerEventType())

    def __init__(self, fn, *args, **kwargs):
        QtCore.QEvent.__init__(self, InvokeEvent.EVENT_TYPE)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs


class Invoker(QtCore.QObject):
    def event(self, event):
        event.fn(*event.args, **event.kwargs)

        return True

_invoker = Invoker()


def invoke_in_main_thread(fn, *args, **kwargs):
    QtCore.QCoreApplication.postEvent(_invoker,InvokeEvent(fn, *args, **kwargs))