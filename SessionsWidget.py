# -*- coding: utf-8 -*-
import sys
import random
from PySide6 import QtCore, QtWidgets, QtGui
from urllib.request import *
import datetime
from DispatchClient import *

class SessionsWidget(QtWidgets.QWidget):

    getHallSessionSignal = QtCore.Signal(QObject)

    def __init__(self, parent=None):
        super().__init__(parent)

        #self.layout = QtWidgets.QFormLayout(self)
        self.layout = QtWidgets.QGridLayout(self)


    def showSessions(self,sessions):

        self.closeButton = QtWidgets.QPushButton(self)
        self.closeButton.setText("X")
        self.closeButton.setStyleSheet("background-color: grey;")
        self.closeButton.clicked.connect(self.closeHallWidget)
        self.closeButton.setGeometry(10,10,20,20)

        parentWidth = self.parent().width()
        numberOfWidgets = 5

        widgetWidth = float(parentWidth) / numberOfWidgets - numberOfWidgets * 5
        widgetHeightRatio = 6.0

        sessions_dict = {}

        for session in sessions:
            sessionDateTime = datetime.datetime.utcfromtimestamp(session['start_time']['$date'] / 1e3)
            date_key = str(sessionDateTime.date())
            if date_key in sessions_dict:
                sessions_dict[date_key].append(session)
            else:
                sessions_dict[date_key] = []
                sessions_dict[date_key].append(session)

        cursor_x = 20
        cursor_y = 40

        for date_key,sessions in sessions_dict.items():

            dateBtn = QtWidgets.QLabel(self)
            dateBtn.setText(date_key)
            dateBtn.setStyleSheet("color:white;")
            dateBtn.setGeometry(cursor_x,cursor_y,100,20)
            #self.layout.addWidget(dateBtn)

            cursor_y += 25

            for session in sessions:

                #new row if outside of screen
                if cursor_x + widgetWidth >= self.parent().width():
                    cursor_y += (widgetWidth / widgetHeightRatio + 5)
                    cursor_x = 20

                sessionDateTime = datetime.datetime.utcfromtimestamp(session['start_time']['$date'] / 1e3)
                sessionBtn = QtWidgets.QPushButton(self)
                sessionBtn.pk = session['_id']
                sessionBtn.hall_id = session['hall_id']['$oid']
                sessionText = str(sessionDateTime.time())[-8:5] +' '+session['session_language'].upper()
                if 'is_3d' in session and session['is_3d'] == True:
                    sessionText += ' 3D'
                if 'is_atmos' in session and session['is_atmos'] == True:
                    sessionText += ' ATMOS'

                if 'is_imax' in session and session['is_imax'] == True:
                    sessionText += ' IMAX'

                if 'subtitle_language' in session and session['subtitle_language']:
                    sessionText += ' SUB ' + session['subtitle_language']

                sessionBtn.setText(sessionText)
                #sessionBtn.setFixedSize(widgetWidth,widgetWidth / widgetHeightRatio)
                sessionBtn.setGeometry(cursor_x,cursor_y,widgetWidth, widgetWidth / widgetHeightRatio)
                sessionBtn.setStyleSheet("background-color: rgba(230, 0, 115, 200); color:white; border-radius: 9px; text-align:center;")
                sessionBtn.clicked.connect(self.sessionClicked)  
                #self.layout.addWidget(sessionBtn)
                cursor_x += (widgetWidth + 5)

            cursor_y += (widgetWidth / widgetHeightRatio + 5)
            cursor_x = 20


        # for session in sessions:
        #     #print(session)
        #     sessionDateTime = datetime.datetime.utcfromtimestamp(session['start_time']['$date'] / 1e3)
        #     sessionBtn = QtWidgets.QPushButton()
        #     sessionBtn.pk = session['_id']
        #     sessionBtn.hall_id = session['hall_id']['$oid']
        #     sessionBtn.setText(str(sessionDateTime.time())[-8:5])
        #     sessionBtn.setFixedSize(widgetWidth,widgetWidth / widgetHeightRatio)
        #     sessionBtn.setStyleSheet("background-color: rgba(230, 0, 115, 200); color:white; border-radius: 9px; text-align:center;")
        #     sessionBtn.clicked.connect(self.sessionClicked)            
        #     #sessionBtn.setSizePolicy(QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Minimum)

        #     dateBtn = QtWidgets.QLabel(str(sessionDateTime.date()))
        #     dateBtn.setStyleSheet("color:white;")
        #     self.layout.addRow(dateBtn,sessionBtn)


        # self.setStyleSheet("""background-color: rgba(100, 100, 100, 250);""")

    def closeHallWidget(self):
        self.hide()

    def sessionClicked(self):
        print("clicked ", self.sender().pk)
        dc = DClient()
        seats = json.loads(dc.getSeats(self.sender().hall_id))
        #print(seats)
        respo = json.loads(dc.getSeatStates(self.sender().pk))
        self.sender().seats = seats
        self.sender().seatstates = respo
        self.getHallSessionSignal.emit(self.sender())

if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = SessionsWidget()
    widget.show()

    sys.exit(app.exec_())