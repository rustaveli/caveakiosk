# -*- coding: utf-8 -*-

from DispatchClient import *
import sys
import random
from PySide6 import QtCore, QtWidgets, QtGui
from MovieWidget import *
from BarProductWidget import *
from SessionsWidget import *
from HallWidget import *
import globals as glob
from globals import *

class MainWidget(QtWidgets.QWidget):

    def __init__(self):
        super().__init__()

        self.getMoviesButton = CQPushButton("MOVIES")
        self.getProducts = CQPushButton("CONCESSION")

        self.getMoviesButton.setMinimumHeight(50)
        self.getProducts.setMinimumHeight(50)

        self.mainLayout = QtWidgets.QVBoxLayout(self)
        
        # widgets
        self.scroll_panel = QtWidgets.QWidget()
        self.scroll_panel_layout = QtWidgets.QVBoxLayout(self.scroll_panel)
        self.scroll_panel_layout.setContentsMargins(0,0,0,0)
        self.scroll_area = QtWidgets.QScrollArea()
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.scroll_area.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.scroll_area.setWidget(self.scroll_panel)
        self.scroll_area.clearHint = 'scroll'

        self.layout = QtWidgets.QVBoxLayout()
        self.mainLayout.addLayout(self.layout)

        self.layout.addWidget(self.scroll_area)   
        
        # self.posterWidget = QtWidgets.QWidget(self)
        # self.posterWidget.resize(500,500)
        # # layout
        # self.layout = QtWidgets.QGridLayout(self.posterWidget)
        # self.layout.setContentsMargins(0,0,0,0)
        # self.layout.addWidget(self.scroll_area)

        self.bottomButtonsLayout = QtWidgets.QHBoxLayout()
        self.mainLayout.addLayout(self.bottomButtonsLayout)

        self.bottomButtonsLayout.addWidget(self.getMoviesButton)
        self.bottomButtonsLayout.addWidget(self.getProducts) 

        self.getMoviesButton.clicked.connect(self.getMovies)
        self.getProducts.clicked.connect(self.getBarProducts)
        self.scroll_panel.setStyleSheet("background-color: black;")
        self.setStyleSheet("background-color: grey;")
        self.changeButtonStyle(self.getMoviesButton,"grey")
        self.changeButtonStyle(self.getProducts, "grey")
        #self.getMoviesButton.updateStyleSignal = QtCore.Signal(QtCore.QObject,QtCore.QObject)
        #print(type(self.getMoviesButton.updateStyleSignal))
        self.getMoviesButton.updateStyleSignal.connect(self.changeButtonStyle)
        #self.getProducts.updateStyle.connect(self.changeButtonStyle)

        # self.setFixedSize(500, 500)
        #self.resize(1520, 800)
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        self.setWindowTitle('CaveaKiOSk')

    @QtCore.Slot()
    def changeButtonStyle(self,button,color):
        #print("Entered Change")
        button.setStyleSheet("""background: %s;
                                        border-radius: 19px;
                                        border-style:solid;
                                        border-width: 2px;
                                        border-color: qlineargradient(x1:0, y1:0, x2:1, y2:0, stop: 0 blue, stop: 1 red) red
                                        qlineargradient(x1:0, y1:0, x2:1, y2:0, stop: 0 blue, stop: 1 red)
                                        blue;""" % (color))


    @QtCore.Slot()
    def getMovies(self):
        #glob.invoke_in_main_thread(self.changeButtonStyle,self.getMoviesButton,"black")
        #self.changeButtonStyle(self.getMoviesButton,"black")
        self.getMoviesButton.updateStyleSignal.emit(self.getMoviesButton,"black")
        self.clearLayout(self.scroll_panel_layout)
        dc = DClient()
        dc.loginUser('ubuntu','cikvicikvi','5528a9b69561673f17a8e8df')
        respo = json.loads(dc.getMovies('5d8fb2ea95616708ef1cd194'))
        #print(respo)
        data = json.loads(respo['data'])
        count = 0

        layout = QtWidgets.QHBoxLayout()
        self.scroll_panel_layout.addLayout(layout)
        #self.layout

        areaWidth = self.frameGeometry().width()
        cols = 5
        heightRatio = 300 / 170
        width = areaWidth / cols - cols
        height = width * heightRatio

        for movie in data:
            if count % cols == 0 and count > 0:
                newRow = QtWidgets.QHBoxLayout()
                self.scroll_panel_layout.addLayout(newRow)
                layout = newRow
            mvWidget = MovieWidget()
            #mvWidget.clearHint = ''
            #mvWidget.posterURL = movie['poster']
            mvWidget.setFixedSize(width,height)
            mvWidget.setPoster(movie)
            layout.addWidget(mvWidget)
            
            mvWidget.setStyleSheet("background-color: white;")
            mvWidget.getSessionsSignal.connect(self.getSessions)

            count += 1

        self.scroll_panel.resize(500,500)
        #self.changeButtonStyle(self.getMoviesButton,"grey")
        self.getMoviesButton.updateStyleSignal.emit(self.getMoviesButton,"grey")
        print("done")

    @QtCore.Slot()
    def getHall(self,sessionButton):
        print("Got hall session signal")
        scaleFactor = float(self.width())/110.0
        print(scaleFactor)
        self.hallWidget = HallWidget(self)
        self.hallWidget.move(0, 0)
        self.hallWidget.resize(self.width(), self.height())
        self.hallWidget.setAttribute(Qt.WA_StyledBackground, True)
        self.hallWidget.setStyleSheet("background-color: rgba(0,0,0,230);")
        self.hallWidget.drawHall(json.loads(sessionButton.seats['data']),scaleFactor)
        self.hallWidget.setSeatStates(json.loads(sessionButton.seatstates['data']))
        self.hallWidget.show()

    def getSessions(self,movieId):
        print("Get Sessions Pressed with ID " + movieId)
        dc = DClient()
        respo = json.loads(dc.getSessions('5d8fb2ea95616708ef1cd194',movieId))
        if respo and 'status' in respo and respo['status'] == 'OK!':
            data = json.loads(respo['data'])
            #sessions = SessionsWidget()
            self._popframe = SessionsWidget(self)
            self._popframe.setAttribute(Qt.WA_StyledBackground, True)
            self._popframe.setStyleSheet("background-color: rgba(0, 0, 0, 200);")
            self._popframe.getHallSessionSignal.connect(self.getHall)
            self._popframe.move(0, 0)
            self._popframe.resize(self.width(), self.height())
            #self._popframe.SIGNALS.CLOSE.connect(self._closepopup)
            self._popframe.showSessions(data)
            self._popflag = True
            self._popframe.show()

            #sessions.showSessions(data)
            #self.mainLayout.addWidget(sessions)

        #print(respo)

    @QtCore.Slot()
    def getBarProducts(self):
        #self.changeButtonStyle(self.getProducts,"black")
        self.clearLayout(self.scroll_panel_layout)
        dc = DClient()
        #dc.loginUser('ubuntu','cikvicikvi','5528a9b69561673f17a8e8df')
        respo = json.loads(dc.getBarProducts())
        data = respo['data']
        count = 0

        layout = QtWidgets.QHBoxLayout()
        self.scroll_panel_layout.addLayout(layout)

        for product in data:
            if count % 5 == 0 and count > 0:
                newRow = QtWidgets.QHBoxLayout()
                self.scroll_panel_layout.addLayout(newRow)
                layout = newRow
            mvWidget = BarProductWidget()
            mvWidget.clearHint = ''
            mvWidget.setPoster(product)
            layout.addWidget(mvWidget)
            mvWidget.setFixedSize(300,300)
            mvWidget.setStyleSheet("background-color: white;")
            
            count += 1

        self.scroll_panel.resize(500,500)
        #self.changeButtonStyle(self.getProducts,"grey")
        print("done")


    def clearLayout(self,layout):
        for i in reversed(range(layout.count())): 
            for k in reversed(range(layout.itemAt(i).count())):
            
            #layout.removeItem(layout.itemAt(i))
                print("removing layout widget")
                layout.itemAt(i).itemAt(k).widget().setParent(None)



if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = MainWidget()
    widget.showFullScreen()

    sys.exit(app.exec_())






