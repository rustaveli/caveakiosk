import socket
import sys
import json
from twisted.internet import protocol, reactor, threads

import random
import string
from PySide6.QtCore import *
import globals as glob
import logging
#from MainWidjet import MainWidget
logging.basicConfig(level=logging.DEBUG)

class DispatchSocketClientProtocol(protocol.Protocol):
    """Once connected, send a message, then print the result."""

    def connectionMade(self):

        DispatchSocketClient.protocolInstance = self;
        print ("CONNECTED " * 10)
        user = None
        session = None
        if not DispatchSocketClient.user:
            DispatchSocketClient.user = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5)) 
        if not DispatchSocketClient.session:
            DispatchSocketClient.session = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5)) 
        if not DispatchSocketClient.token:
            DispatchSocketClient.token = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
        user = str(DispatchSocketClient.user)
        session = str(DispatchSocketClient.session)
        token = str(DispatchSocketClient.token)
        self.transport.write(json.dumps({"command":"subscribe","user":user,"session":session, "token":token}) + '\n')
    
    def dataReceived(self, data):

        print ("Data Received ", data)
        print (type(data))
        seatData = json.loads(json.loads(json.dumps(data)))
        print ('json data: ', type(seatData))
        glob.twistedhelperGlobal.emitSocketData(seatData)
    
    def connectionLost(self, reason):
        print ("connection lost")

class TwistedHelper(QObject):
    TwistedCallSignal = Signal(dict)

    def __init__(self, parent=None):
        super(TwistedHelper, self).__init__(parent)

    def emitSocketData(self, seatData):
        print ('EMIT SIGNAL !!! ')
        self.TwistedCallSignal.emit(seatData)


class DispatchSocketClient(protocol.ClientFactory):
    protocol = DispatchSocketClientProtocol
    protocolInstance = None
    token = None
    user = None
    session = None
    seats = "[]"
    SERVER_IP =  glob.config.get('dispatch', 'SERVER_IP')

    def clientConnectionFailed(self, connector, reason):
        print ("Connection failed - goodbye!")
        reactor.stop()
    
    def clientConnectionLost(self, connector, reason):
        print ("Connection lost - goodbye!")
        reactor.stop()    

    def startSocket(self):
        sock_address = self.SERVER_IP.split('/')
        sock_address = sock_address[2]
        print ("Socket address is ==== %s" % sock_address)

        if ':' in sock_address:
            sock_address = sock_address[0:sock_address.index(':')]

        reactor.connectTCP(sock_address, 9001, self)
        reactor.run(installSignalHandlers=False)

    def subscribe_to_session(self, user=None, session=None, token=None):
        if not user:
            user = u'%s' % DispatchSocketClient.user
            user = user.decode('utf-8')
        if not session:
            session = u'%s' % DispatchSocketClient.session
            session = session.decode('utf-8')
        if not token:
            token = DispatchSocketClient.token
        fullcommand = (json.dumps({"command":"subscribe","user":user,"session":session, "token":token}) + '\n')

        try:
            reactor.callFromThread(DispatchSocketClient.protocolInstance.transport.write, fullcommand)
        except Exception as e:
            print (str(e))
