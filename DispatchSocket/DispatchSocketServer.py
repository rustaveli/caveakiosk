
# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.


from twisted.internet import reactor, protocol
import sys
import json




class DispatchSocketServer(protocol.Protocol):
    
    def connectionMade(self):
        #self.transport.write("""connected""")
        self.user = None
        self.session = None
        self.factory.serverInstances.append(self)
        print "clients are ", self.factory.serverInstances
        #d =  self.transport.getHost () ; print "Host ",d.type, d.host, d.port
        #p = self.transport.getPeer() ; print "Peer ", self.transport.getPeer(), p.type, p.port
    def connectionLost(self, reason):
            self.factory.serverInstances.remove(self)
            print "Connection Lost"
    
    def dataReceived(self, data):
        
        try:
            
            linesList = data.split('\n')
            for line in linesList:
                print "ReceivedLine ", line
                print "\n"
                messageJson = json.loads(line.rstrip())
                session = messageJson['session']
                user = messageJson['user']
                if messageJson['command'] == 'subscribe':
                    self.user = user
                    self.session = session
                elif messageJson['command'] == 'sell':
                    seats = messageJson['seats']
                    for serverInstance in self.factory.serverInstances:
                        if serverInstance.user != user and serverInstance.session == session:
                            sendJSON = messageJson
                            sendJSON['user'] = None
                            serverInstance.transport.write(json.dumps(sendJSON))
                elif messageJson['command'] == 'book':
                    seats = messageJson['seats']
                    for serverInstance in self.factory.serverInstances:
                        if serverInstance.user != user and serverInstance.session == session:
                            sendJSON = messageJson
                            sendJSON['user'] = None
                            serverInstance.transport.write(json.dumps(sendJSON))
                elif messageJson['command'] == 'free':
                    seats = messageJson['seats']
                    for serverInstance in self.factory.serverInstances:
                        if serverInstance.user != user and serverInstance.session == session:
                            sendJSON = messageJson
                            sendJSON['user'] = None
                            serverInstance.transport.write(json.dumps(sendJSON))
        except:
            print data
            print "error parsing"
        #"As soon as any data is received, write it back."
        #self.transport.write(data)

    def message(self, message):
        self.transport.write(message + '\n')
        
        


def main():
    """This runs the protocol on port 8000"""
    factory = protocol.ServerFactory()
    factory.protocol = DispatchSocketServer
    factory.serverInstances = []
    #reactor.listenSSL(22109, factory, ssl.DefaultOpenSSLContextFactory('self.key', 'self.crt'))
    #reactor.listenSSL(22109, factory, ssl.DefaultOpenSSLContextFactory('/usr/share/ca-certificates/extra/KuzkaServer.key', '/usr/share/ca-certificates/extra/kuzkaserver_com.crt'))
    reactor.listenTCP(8000,factory)
    reactor.run()

# this only runs if the module was *not* imported
if __name__ == '__main__':
    try:
        main()
    except:
        sys.exit()
